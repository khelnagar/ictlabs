// File by Khaled Elnagar

function makeStringId(idLength) {
	// It takes a valid number and returns a string id of length idLength //

	if (isNaN(idLength)) {
		throw Error('Enter a valid number.');
	}

	const possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789@#$%^&?=_-!';
	
	var stringId = '';
	for (var i = 0; i < idLength; i++) {
		let randomIdx = Math.floor(Math.random() * possibleChars.length);
		stringId += possibleChars.charAt(randomIdx);
	}
	
	return stringId;
}

console.log(makeStringId(6));